Rails.application.routes.draw do
  resources :bank_notes
  resources :transactions
  resources :accounts
  devise_for :users,
             defaults: { format: :json },
             path: '',
             controllers: {
               registrations: 'users/registrations',
               sessions: 'users/sessions'
             }

  mount Interface::V1::Base => '/api'
end
