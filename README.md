# ATM Service

## Installation

this project contains a docker compose file, to make it easier to implement in your computer.

this first command will create two services the database and the main application
```shell
$ docker-compose -f docker-compose up -d
```

now we need to create the database, tables and the initial data
```shell
$ docker-compose run app rails db:create db:migrate db:seed
```

after these steps we can start making request to our ATM service

## Docs

this section contains the information about how to implement and start using this service

### Sign In

**Request**
```shell
POST http://localhost:3000/sign_in

Content-Type: application/json

{
  "user": {
    "email": "test@email.com",
    "password": "12345678"
  }
}
```

**Response**

```shell
Authorization: Bearer <JWT>

{
  "data": {
    "id": UUID,
    "type": "users",
    "attributes": {
      "id": UUID,
      "email": "test@example.com"
    },
    "links": {
      "self": UUID
    }
  },
  "jsonapi": {
    "version": "1.0"
  }
}

```

### Get Bank Notes

This will return a property data containing all the existent bank notes with its value and stock

**Request**
```shell
GET http://localhost:3000/api/v1/bank_notes

Authorization: Bearer JWT
```

**Response**
```shell
{
  "data": [
    {
      "id": Integer,
      "value": Integer
      "stock": Integer,
      "currency_id": Integer,
      "currency_code": String
    }
  ],
  "message": "Success"
}
```

### Update Bank Note

this allow us to update the stock of an existent bank note

**Request**
```shell
PUT http://localhost:3000/api/v1/bank_notes/:id

Authorization: Bearer JWT

{
  "stock": Integer
}

```

**Response**
```shell
{
  "stock": Integer,
  "value": Integer,
  "id": Integer,
  "currency_id": Integer,
  "created_at": DateTime String,
  "updated_at": DateTime String 
}
```


### Create Bank Note

this allow us to create a bank note sending the value, stock and currency_name

**Request**
```shell
POST http://localhost:3000/api/v1/bank_notes

Authorization: Bearer JWT

{
  "currency_name": String,
  "stock": Integer,
  "value": Integer | Float
}

```

**Response**
```shell
{
  "id": Integer,
  "value": Integer,
  "stock": Integer,
  "currency_code": String
}
```

### Create Withdraw Transaction

this will create a withdrawal transaction which will check the user balance, reduce his account balance, and return the used bank_notes for the operation

**Request**
```shell
POST http://localhost:3000/api/v1/transactions

Authorization: Bearer JWT

{
	"transaction_type_id": Integer,
	"data": {
		"amount": Integer | Float
	}
}
```

**Response**
```shell
{
  "message": String,
  "data": {
    "previous_balance": Float,
    "withdraw_amount": Float,
    "new_balance": Float,
    "bank_notes": {
      "10": Integer,
      [Bank Note]: Count
    },
    "success": Boolean
  }
}
```

### Create Deposit Transaction

this will create a deposit transaction which will modify the user account balance

**Request**
```shell
POST http://localhost:3000/api/v1/transactions

Authorization: Bearer JWT

{
	"transaction_type_id": Integer,
	"data": {
		"amount": Integer | Float
	}
}
```

**Response**
```shell
{
  "message": String,
  "data": {
    "previous_balance": Float,
    "deposit_amount": Float,
    "new_balance": Float,
    "success": Boolean
  }
}
```