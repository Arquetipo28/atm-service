module Interface::Helpers
  module AuthHelper
    def validate_authorization!
      throw 'No authorization token sent' unless headers['Authorization']

      # get token from header
      token = headers['Authorization'].split(' ')[1]
      decoded_token = JWT.decode(token, Rails.application.credentials.devise[:jwt_secret_key])
      # get the user uuid from the subject of the JWT
      subject_uuid = decoded_token[0]['sub']
      puts "TOMATO: #{subject_uuid}"
      @current_user = User.find(subject_uuid)
    rescue JWT::ExpiredSignature
      throw 'Authorization token expired'
    rescue JWT::DecodeError
      throw 'Token not valid'
    end
  end
end