class Interface::V1::Base < Grape::API
  content_type :json, 'application/json; charset=UTF-8'
  format :json
  version 'v1'
  helpers Interface::Helpers::AuthHelper

  before do
    validate_authorization!
  end

  mount Interface::V1::BankNotes
  mount Interface::V1::Transactions
end