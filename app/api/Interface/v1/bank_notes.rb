class Interface::V1::BankNotes < Grape::API
  resource :bank_notes do
    desc 'Returns all bank notes' do
      detail 'Returns all existent bank notes'
      http_codes [
                   {
                     code: 200,
                     message: 'Success',
                     data: []
                   }
                 ]
    end
    get do
      bank_notes = BankNote.includes(:currency).select(:id, :value, :stock, :currency_id).all.map do |bank_note|
        bank_note.as_json.merge(currency_code: bank_note.currency.name)
      end

      { data: bank_notes, message: 'Success' }
    end

    desc 'Creates a new bank note' do
      detail 'Creates a new bank note with currency, stock and value'
      http_codes [
                   {
                     code:    200,
                     message: 'Success',
                   }
                 ]
      nickname 'createBankNote'
    end

    params do
      requires :currency_name, type: String, desc: 'Currency code.'
      requires :stock, type: Integer, desc: 'Bank note initial stock.'
      requires :value, type: Float, desc: 'Bank note real value'
    end
    post '/' do
      currency = Currency.find_by_name(params[:currency_name])
      new_banknote = BankNote.create(currency: currency, value: params[:value], stock: params[:stock])
      new_banknote.slice(:id, :value, :stock).merge(currency_code: currency.name)
    end

    route_param :id do
      desc 'Edit an existent bank note' do
        detail 'Edit a bank note'
        http_codes [
                     {
                       code: 200,
                       message: 'Success'
                     }
                   ]
        nickname 'editBankNote'
      end

      params do
        requires :id, type: Integer, desc: 'ID of the bank note to edit'
        optional :stock, type: Integer, desc: 'new existence stock of the bank note'
      end
      put do
        bank_note = BankNote.find(params[:id])
        update_params = params.slice(:stock)
        bank_note.update(update_params)

        bank_note
      end
    end
  end
end