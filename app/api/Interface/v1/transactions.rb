class Interface::V1::Transactions < Grape::API
  resource :transactions do
    desc 'Executes a new transaction' do
      http_codes [
                   {
                     code: 200,
                     message: 'Success',
                   },
                   {
                     code: 200,
                     message: 'Failure',
                     errors: []
                   }
                 ]
      nickname 'executeTransaction'
    end
    params do
      requires :transaction_type_id, type: Integer, desc: 'ID of the transaction type'
      requires :data, type: Hash, desc: 'Data for the transaction execution'
    end

    post do
      transaction_data = params[:data]
      service = Transactions::Execute.new(
        @current_user,
        transaction_data,
        transaction_type_id: params[:transaction_type_id]
      )
      service.call
      if service.errors.present?
        { message: 'Failure', data: {}, errors: service.errors }
      else
        { message: 'Success', data: service.output }
      end
    end
  end
end