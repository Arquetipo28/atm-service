class SerializableUser < JSONAPI::Serializable::Resource
  type 'users'

  attributes :id
  attributes :email

  link :self do
    @url_helpers.full_url_for(@object.id)
  end
end