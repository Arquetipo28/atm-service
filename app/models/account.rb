class Account < ApplicationRecord
  self.locking_column = :lock_version

  belongs_to :user
  belongs_to :currency

  has_many :transactions

  validates :balance, presence: true, numericality: true
end
