class Transaction < ApplicationRecord
  belongs_to :account
  belongs_to :transaction_type
  validates :transaction_type, presence: true
end
