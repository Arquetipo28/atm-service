class BankNote < ApplicationRecord
  belongs_to :currency

  validates :stock, presence: true, numericality: true
  validates :value, presence: true, numericality: true
end
