class TransactionType < ApplicationRecord
  has_many :transactions
  enum :t_type => { withdrawal: 0, deposit: 1 }, _prefix: true
end
