class Currency < ApplicationRecord
  validates :name, presence: true
  validates :label, presence: true

  has_many :bank_notes
  has_many :accounts
end
