class Transactions::Execute::Deposit
  attr_accessor :result, :errors

  def initialize(transaction_data, transaction_type, account:)
    @transaction_data = transaction_data
    @transaction_type = transaction_type
    @account = account
    @result = nil
    @errors = []
  end

  def execute
    @transaction = Transaction.new(transaction_type: @transaction_type, account: @account)
    Account.transaction do
      current_balance = @account.balance
      deposit_amount = @transaction_data[:amount]
      new_balance = current_balance + deposit_amount
      data = {
        previous_balance: current_balance.to_f,
        deposit_amount: deposit_amount.to_f,
        new_balance: new_balance.to_f,
        success: @errors.empty?
      }

      @transaction.data = data
      @account.balance = new_balance
      @account.save
      @result = data
    end
    @transaction.save!
  end
end