class Transactions::Execute::Withdrawal
  attr_accessor :result, :errors

  def initialize(transaction_data, transaction_type, account:)
    @transaction_data = transaction_data
    @transaction_type = transaction_type
    @account = account
    @result = nil
    @errors = []
  end

  def execute
    @transaction = Transaction.new(transaction_type: @transaction_type, account: @account)
    Account.transaction do
      current_balance = @account.balance
      withdraw_amount = @transaction_data[:amount]
      new_balance = current_balance - withdraw_amount

      calculate_bank_notes(withdraw_amount)

      if !valid_withdraw?(current_balance, withdraw_amount)
        return @errors << 'The account does not have enough balance'
      elsif @bank_notes.empty?
        return @errors << 'This machine has not the necessary notes for this operation'
      else
        # if no errors remove bank notes from existence
        reduce_bank_notes
      end

      data = {
        previous_balance: current_balance.to_f,
        withdraw_amount: withdraw_amount.to_f,
        new_balance: new_balance.to_f,
        bank_notes: @bank_notes,
        success: @errors.empty?
      }

      @transaction.data = data
      @account.balance = new_balance
      @account.save
      @result = data
    end
    @transaction.save!
  end

  private

  def calculate_bank_notes(amount)
    return @bank_notes if @bank_notes.present?

    @bank_notes = {}
    notes = BankNote.select(:stock, :value).where(currency_id: @account.currency_id).sort { |x, y| y.value - x.value }
    current_amount = amount
    notes.each do |note|
      evaluation = (current_amount / note.value).floor
      next unless evaluation > 0
      next if !note.stock || note.stock < 1 || evaluation > note.stock

      @bank_notes[:"#{note.value}"] = evaluation
      current_amount -= note.value * evaluation
    end

    # transaction not completed
    @bank_notes = {} if !current_amount.zero?

    @bank_notes
  end

  def reduce_bank_notes
    @bank_notes.each do |key, value|
      bank_note = BankNote.find_by(currency: @account.currency, value: key.to_s.to_i)
      bank_note.update(stock: bank_note.stock - value)
    end
  end

  def valid_withdraw?(current_balance, withdraw_amount)
    current_balance >= withdraw_amount
  end
end