class Transactions::Execute
  attr_accessor :output, :errors

  def initialize(user, transaction_data, transaction_type_id:)
    @user = user
    @transaction_data = transaction_data
    @transaction_type_id = transaction_type_id
  end

  def call
    transaction_klass = get_transaction_klass.new(@transaction_data, transaction_type, account: @user.account)
    transaction_klass.execute
    @output = transaction_klass.result
    @errors = transaction_klass.errors
  rescue StandardError => e
    puts e
    @output = nil
    @errors = ['Unexpected error']
  end

  def get_transaction_klass
    constant = transaction_type.t_type.camelize
    "Transactions::Execute::#{constant}".constantize
  end

  def get_transaction_options
  end

  def transaction_type
    @transaction_type ||= TransactionType.select(:t_type).find(@transaction_type_id)
  end
end