# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# ==> Default users
test_user = User.find_or_create_by(email: 'test@example.com') do |u|
  u.password = '12345678'
end

# ==> transaction_types creation
%i[withdrawal deposit].each do |transaction_type|
  TransactionType.find_or_create_by(t_type: transaction_type)
end

# ==> Currencies
[
  {
    name: 'MXN',
    label: 'Peso Mexicano',
    bank_notes: [
      {
        value: 10,
        stock: 100
      }
    ]
  }
].each do |currency_data|
  Currency.find_or_create_by(name: currency_data[:name]) do |currency|
    currency.label = currency_data[:label]
    currency_data[:bank_notes].each do |bank_note_data|
      currency.bank_notes.find_or_initialize_by(currency: currency, value: bank_note_data[:value]) do |bank_note|
        bank_note.stock = bank_note_data[:stock]
      end
    end
  end
end

# ==> Default accounts
Account.find_or_create_by(user: test_user) do |account|
  account.balance = 100.0
  account.currency = Currency.find_by(name: 'MXN')
  account.save
end