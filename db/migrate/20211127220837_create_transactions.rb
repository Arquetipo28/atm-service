class CreateTransactions < ActiveRecord::Migration[6.1]
  def change
    create_table :transactions do |t|
      t.jsonb :data, null: false
      t.references :account, type: :uuid

      t.timestamps
    end
  end
end
