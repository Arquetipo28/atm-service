class CreateTransactionTypes < ActiveRecord::Migration[6.1]
  def change
    create_table :transaction_types do |t|
      t.integer :t_type

      t.timestamps
    end

    add_reference :transactions, :transaction_type, foreign_key: true
  end
end
