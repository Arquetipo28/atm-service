class AddCurrencyToAccounts < ActiveRecord::Migration[6.1]
  def change
    add_reference :accounts, :currency, foreign_key: true
  end
end
