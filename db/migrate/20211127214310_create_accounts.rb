class CreateAccounts < ActiveRecord::Migration[6.1]
  def change
    create_table :accounts, id: :uuid do |t|
      t.decimal :balance, default: 0
      t.string  :lock_version

      t.references :user, type: :uuid

      t.timestamps
    end
  end
end
