class CreateBankNotes < ActiveRecord::Migration[6.1]
  def change
    create_table :bank_notes do |t|
      t.integer :stock
      t.integer :value
      t.references :currency

      t.timestamps
    end
  end
end
