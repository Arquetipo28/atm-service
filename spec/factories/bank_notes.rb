FactoryBot.define do
  factory :bank_note do
    stock { 10 }
    value { 10 }
  end
end