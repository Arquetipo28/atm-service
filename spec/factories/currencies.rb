FactoryBot.define do
  factory :currency do
    name { Faker::Currency.code }
    label { Faker::Currency.name }

    after :create do |currency, _evaluator|
      create_list(:bank_note, 5, currency: currency) do |bank_note, i|
        bank_note.value = bank_note.value * (i + 1)
        bank_note.save
      end
    end
  end
end