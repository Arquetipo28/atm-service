FactoryBot.define do
  factory :transaction_type do
    trait :withdrawal do
      t_type { :withdrawal }
    end

    trait :deposit do
      t_type { :deposit }
    end
  end
end