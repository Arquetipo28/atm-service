FactoryBot.define do
  factory :account do
    balance { 100.0 }
    currency
    user

    after :create do |account, evaluator|
      if evaluator.balance
        account.balance = evaluator.balance
      end
    end
  end
end