require 'rails_helper'

RSpec.describe Transactions::Execute do
  describe '.call' do
    describe 'withdrawal' do
      context 'when user has enough balance' do
        let(:user) { create(:user, :user_with_account) }
        let(:transaction_type) { create(:transaction_type, :withdrawal) }
        let(:transaction_data) { { amount: 50.0 } }
        let(:service) { described_class.new(user, transaction_data, transaction_type_id: transaction_type.id) }

        it 'should withdraw 50.0' do
          expect do
            service.call
          end.to change { user.account.balance }.by(-50.0)
        end

        it 'should create a transaction with new_balance 50' do
          service.call
          transaction = user.account.transactions.last
          expect(transaction.data['new_balance']).to eq 50.0
        end

        it 'should return bank_notes including 50' do
          service.call
          transaction = user.account.transactions.last
          expect(transaction.data['bank_notes']).to include("50")
        end

        it 'should return bank_notes including 50 equal 1' do
          service.call
          transaction = user.account.transactions.last
          expect(transaction.data['bank_notes']['50']).to eq 1
        end

        context 'when bank notes are not enough' do
          let(:user) { create(:user, :user_with_account) }
          let(:transaction_type) { create(:transaction_type, :withdrawal) }
          let(:transaction_data) { { amount: 27.0 } }
          let(:service) { described_class.new(user, transaction_data, transaction_type_id: transaction_type.id) }

          it 'should raise machine error' do
            service.call
            puts service.errors
            expect(service.errors[0]).to eq 'This machine has not the necessary notes for this operation'
          end
        end
      end

      context 'when user has not enough balance' do
        let(:user) do
          create(:user, {
            account_attributes: { balance: 40.0, currency: create(:currency) }
          })
        end
        let(:transaction_type) { create(:transaction_type, :withdrawal) }
        let(:transaction_data) { { amount: 50.0 } }
        let(:service) { described_class.new(user, transaction_data, transaction_type_id: transaction_type.id) }

        it 'output should return nil' do
          service.call
          expect(service.output).to be_nil
        end

        it 'should contain not enough balance error' do
          service.call
          expect(service.errors[0]).to eq 'The account does not have enough balance'
        end
      end
    end

    describe 'deposit' do
      context 'with valid data' do
        let(:user) { create(:user, :user_with_account) }
        let(:transaction_type) { create(:transaction_type, :deposit) }
        let(:transaction_data) { { amount: 50.0 } }
        let(:service) { described_class.new(user, transaction_data, transaction_type_id: transaction_type.id) }

        it 'should add 50 to the account balance' do
          expect do
            service.call
          end.to change { user.account.balance }.by 50
        end

        it 'should create a transaction with new_balance 150' do
          service.call
          transaction = user.account.transactions.last
          expect(transaction.data['new_balance']).to eq 150.0
        end
      end
    end
  end
end